//
//  R.swift
//  Tradiz2
//
//  Created by JIS on 5/9/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

struct R {
    
    struct string {
        
        static let APP_TITLE = "Tradiz"
        static let SIGNUP = "Sign Up"
        
        static let OK = "OK"
        static let CANCEL = "Cancel"
        static let ERROR = "Error Occured!"
        static let CONNECT_FAIL = "Connection to the server failed.\nPlease try again."
        
        static let INPUT_EMAIL = "Please input email."
        static let INVALID_EMAIL = "Invalid Email address."
        static let INPUT_FULLNAME = "Please input Full Name."
        static let INPUT_PASSWORD = "Please input password. \nPassword must be more than 6 letters."
        static let INPUT_PHONE_NUMBER = "Please input Phone Number."
        static let INPUT_ADDRESS = "Please input Address."
        static let INPUT_ABN = "Please input ABN."
        static let INPUT_BUSINESS_NAME = "Please input Business Name."
        static let INPUT_BUSINESS_CONTACT_NUMBER = "Please input Business Contact Number."
        static let SELECT_PHOTO = "Please select photo."
        static let SELECT_COMPANY_LOGO = "Please select Company Logo."
        static let INPUT_WEBSITE_ADDRESS = "Please input Website Address."
        static let SELECT_SKILL = "Please select at least 1 skill."
        static let INPUT_ABOUTYOU = "Please input about you."
        static let INPUT_CARD_NUMBER = "Please input Card Number."
        static let INPUT_EXPIRED_DATE = "Please input Expired date."
        static let INPUT_CCV = "Please input CCV."
        static let WRONG_CARDINFO = "Wrong Card information."
        static let AGREE_TERM = "Please agree with T&C."
        
        static let PHOTO_SOURCE = "Photo Source"
        static let CAMERA = "Camera"
        static let PHOTO_ALBUMS = "Photo Albums"
        
        /*
        static let INPUT_PWD = ""
        static let CONFIRM_PWD = "Please confirm your password."
        
        
        static let INPUT_BIRTHDAY = "Please input your birthday."
        static let INPUT_USERNAME = "Please input username."
        

        static let EMAIL_EXIST = "Email already exists."
        static let USERENAME_EXIST = "The username already exists."
        static let UPLOAD_FAIL = "Upload photo failed."
        static let USER_NOTEXIST = "The email is not registered."
        static let WRONG_PWD = "Invalid password."

        //
        
        */
    }
    
    
}

